package com.book.marker.enumeration;

public enum  BookType {
    EBOOK,
    PAPERBOOK
}
