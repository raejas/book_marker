package com.book.marker.enumeration;

public enum ComponentUnit {
    PART,
    SECTION,
    CHAPTER
}
