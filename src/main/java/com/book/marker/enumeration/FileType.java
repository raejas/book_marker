package com.book.marker.enumeration;

public enum FileType {
    PDF,
    EPUB
}
