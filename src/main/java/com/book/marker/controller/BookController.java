package com.book.marker.controller;

import com.example.demo.model.dto.book.BookCreationDTO;
import com.example.demo.model.dto.book.BookDTO;
import com.example.demo.model.dto.book.BookUpdatedDTO;
import com.example.demo.model.entity.Book;
import com.example.demo.service.BookService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService service;

    @PostMapping
    public BookDTO create(@RequestBody BookCreationDTO dto) {
        return service.create(dto);
    }

    @PostMapping("/names")
    public List<BookDTO> createMultiByNames(@RequestBody Set<String> names) {
        return service.createMultiBooksByName(names);
    }

    @GetMapping
    public List<BookDTO> getListBook() {
        return service.getBookList();
    }

    @GetMapping("/{bookId}")
    public Book getBookById(@PathVariable Integer bookId) throws NotFoundException {
        return service.getById(bookId);
    }

    @PutMapping("/{bookId}")
    public boolean updateBookInfo(@PathVariable Integer bookId, @RequestBody BookUpdatedDTO dto)
            throws NotFoundException {
        return service.update(bookId, dto);
    }

    @DeleteMapping("/{bookId}")
    public boolean deleteBook(@PathVariable Integer bookId) throws NotFoundException {
        return service.delete(bookId);
    }
}
