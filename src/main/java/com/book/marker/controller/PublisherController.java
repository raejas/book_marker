package com.book.marker.controller;

import com.example.demo.model.dto.publisher.PublisherDTO;
import com.example.demo.model.entity.Book;
import com.example.demo.model.entity.Publisher;
import com.example.demo.service.PublisherService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/publisher")
public class PublisherController {

    @Autowired
    private PublisherService service;

    @PostMapping
    public PublisherDTO create(@RequestBody PublisherDTO dto) {
        return service.create(dto);
    }

    @GetMapping
    public List<Publisher> getListBook() {
        return service.getListPublisher();
    }

    @GetMapping("/{publisherId}/books")
    public Set<Book> getListBookofPublisher(@PathVariable Integer publisherId) {
        return service.getListPublisherBooks(publisherId);
    }

    @GetMapping("/{publisherId}")
    public PublisherDTO getBookById(@PathVariable Integer publisherId) {
        return service.getById(publisherId);
    }

    @PutMapping("/{publisherId}")
    public boolean updateBookInfo(@PathVariable Integer publisherId, @RequestBody PublisherDTO dto)
            throws NotFoundException {
        return service.update(publisherId, dto);
    }

    @DeleteMapping("/{publisherId}")
    public boolean deleteBook(@PathVariable Integer publisherId) throws NotFoundException {
        return service.delete(publisherId);
    }
}
