package com.book.marker.controller;

import com.example.demo.model.dto.book.BookDetailDTO;
import com.example.demo.model.dto.book.BookDetailSavingDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/book/{bookId}/detail")
public class BookDetailController {

    @GetMapping
    public BookDetailDTO getBookDetail(@PathVariable Integer bookId) {

    }

    @PostMapping
    public BookDetailDTO createBookDetail(@PathVariable Integer bookId,
                                          @RequestBody BookDetailSavingDTO creationDTO) {

    }

    @PutMapping
    public BookDetailDTO updateBookDetail(@PathVariable Integer bookId,
                                          @RequestBody BookDetailSavingDTO creationDTO) {

    }
}
