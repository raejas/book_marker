package com.book.marker.model;

import com.book.marker.model.entity.BookComponent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class BookTableContent {

    private Integer bookId;

    private String bookName;

    private Set<BookComponent> components;

}
