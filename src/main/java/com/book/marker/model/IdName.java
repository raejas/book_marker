package com.book.marker.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IdName {

    private Integer id;

    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdName idName = (IdName) o;
        return Objects.equals(id, idName.id) &&
                name.equals(idName.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
