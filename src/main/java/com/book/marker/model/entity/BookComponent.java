package com.book.marker.model.entity;

import com.book.marker.enumeration.ComponentUnit;
import com.book.marker.model.dto.book.BookComponentSavingDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class BookComponent extends AbstractEntity{

    private ComponentUnit unit;

    private Integer fromPage;

    private Integer toPage;

    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    public BookComponent(BookComponentSavingDTO savingDTO, Book book) {
        BeanUtils.copyProperties(savingDTO, this);
        this.book = book;
    }
}
