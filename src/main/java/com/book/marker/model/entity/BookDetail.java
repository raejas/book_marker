package com.book.marker.model.entity;

import com.example.demo.enumeration.FileType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
@Getter
@Setter
public class BookDetail extends AbstractEntity{

    private String description;

    private Integer totalPages;

    private FileType fileType;

    private Date publishedDate;

    private String source;

    private String language;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "book_id")
    private Book book;
}
