package com.book.marker.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Bookmark extends AbstractEntity {

    private Integer bookId;

    private Integer userId;

    private Integer fromPage;

    private Integer toPage;

    private Boolean isFinished;

    private String remark;
}
