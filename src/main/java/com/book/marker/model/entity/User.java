package com.book.marker.model.entity;

import com.book.marker.enumeration.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    private String email;

    private Long phone;

    private Date dob;

    private String firstName;

    private String lastName;

    private Gender gender;
}
