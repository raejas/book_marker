package com.book.marker.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Category extends AbstractEntity {

    private String name;

    private Short level;

    private Short parent;

    private String description;
}
