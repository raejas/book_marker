package com.book.marker.model.dto.book;

import com.book.marker.enumeration.BookType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookUpdatedDTO {

    private String name;

    private BookType bookType;

    private Short version;
}
