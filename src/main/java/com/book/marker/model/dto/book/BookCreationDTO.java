package com.book.marker.model.dto.book;

import com.book.marker.enumeration.BookType;
import com.book.marker.model.entity.Author;
import com.book.marker.model.entity.BookDetail;
import com.book.marker.model.entity.Category;
import com.book.marker.model.entity.Publisher;
import com.book.marker.model.entity.Tag;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class BookCreationDTO {

    private String name;

    private BookType bookType;

    private BookDetail bookDetail;

    private Publisher publisher;

    private Set<Author> authors;

    private Category category;

    private Set<Tag> tags;

}
