package com.book.marker.model.dto.book;

import com.book.marker.enumeration.FileType;
import com.book.marker.model.entity.Author;
import com.book.marker.model.entity.Category;
import com.book.marker.model.entity.Publisher;
import com.book.marker.model.entity.Tag;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class BookDetailSavingDTO {

    private Integer totalPages;

    private FileType fileType;

    private Date publishedDate;

    private String source;

    private String language;

    private String description;

    private Publisher publisher;

    private Set<Author> authors;

    private Category category;

    private Set<Tag> tags;

}
