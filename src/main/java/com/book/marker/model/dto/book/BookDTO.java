package com.book.marker.model.dto.book;

import com.book.marker.enumeration.BookType;
import com.book.marker.model.entity.Book;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotBlank;


@Getter
@Setter
@NoArgsConstructor
public class BookDTO {

    private Integer id;

    @NotBlank
    private String name;

    private BookType bookType;

    private Short version;

    public BookDTO(Book book) {
        BeanUtils.copyProperties(book, this);
    }
}
