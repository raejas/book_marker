package com.book.marker.model.dto.book;

import com.book.marker.enumeration.FileType;
import com.book.marker.model.IdName;
import com.book.marker.model.entity.Author;
import com.book.marker.model.entity.Book;
import com.book.marker.model.entity.Category;
import com.book.marker.model.entity.Publisher;
import com.book.marker.model.entity.Tag;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class BookDetailDTO {

    private String name;

    private Integer bookId;

    private String description;

    private Integer totalPages;

    private FileType fileType;

    private Date publishedDate;

    private String source;

    private String language;

    private IdName publisher;

    private Set<IdName> authors;

    private IdName category;

    private Set<IdName> tags;

    public BookDetailDTO(Book book) {
        BeanUtils.copyProperties(book.getBookDetail(), this);
        this.bookId = book.getId();
        this.name = book.getName();
        Publisher publisher = book.getPublisher();
        Category category = book.getCategory();
        Set<Author> authors = book.getAuthors();
        Set<Tag> tags = book.getTags();
        if (Objects.nonNull(publisher)) {
            this.publisher = new IdName(publisher.getId(), publisher.getName());
        }

        if (Objects.nonNull(category)) {
            this.category = new IdName(category.getId(), category.getName());
        }

        if (CollectionUtils.isEmpty(authors)) {
            this.setAuthors(authors.stream()
                    .map(author -> new IdName(author.getId(), author.getName()))
                    .collect(Collectors.toSet()));
        }

        if (CollectionUtils.isEmpty(tags)) {
            this.setTags(tags.stream()
                    .map(tag -> new IdName(tag.getId(), tag.getName()))
                    .collect(Collectors.toSet()));
        }
    }
}
