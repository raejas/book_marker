package com.book.marker.model.dto.book;

import com.book.marker.enumeration.ComponentUnit;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookComponentSavingDTO {

    private ComponentUnit unit;

    private Integer fromPage;

    private Integer toPage;

    private String name;

    private String description;
}
