package com.book.marker.model.dto.author;

import com.book.marker.model.entity.Author;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.Objects;

@Getter
@Setter
public class AuthorDTO {

    private Integer id;

    private String name;

    private String about;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorDTO authorDTO = (AuthorDTO) o;
        return Objects.equals(id, authorDTO.id) ||
                Objects.equals(name, authorDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public AuthorDTO(Author author) {
        BeanUtils.copyProperties(author, this);
    }
}
