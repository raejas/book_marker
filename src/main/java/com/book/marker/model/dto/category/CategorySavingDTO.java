package com.book.marker.model.dto.category;

public class CategorySavingDTO {

    private String name;

    private Short level;

    private Short parent;

    private String description;
}
