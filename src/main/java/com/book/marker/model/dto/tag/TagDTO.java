package com.book.marker.model.dto.tag;

import com.book.marker.model.entity.Tag;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
public class TagDTO {

    private String id;

    private String name;

    public TagDTO(Tag tag) {
        BeanUtils.copyProperties(tag, this);
    }
}
