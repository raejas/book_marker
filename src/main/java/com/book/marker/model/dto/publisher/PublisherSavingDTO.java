package com.book.marker.model.dto.publisher;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublisherSavingDTO {

    private String name;

    private String about;

    private Double totalPublished;

    private String website;
}
