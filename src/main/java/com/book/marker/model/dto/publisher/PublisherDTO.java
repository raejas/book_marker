package com.book.marker.model.dto.publisher;

import com.book.marker.model.entity.Publisher;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
public class PublisherDTO {

    private Integer id;

    private String name;

    private String about;

    private Double totalPublished;

    private String website;

    public PublisherDTO(Publisher publisher) {
        BeanUtils.copyProperties(publisher, this);
    }
}
