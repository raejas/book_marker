package com.book.marker.model.dto.user;

import com.book.marker.enumeration.Gender;
import com.book.marker.model.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.Date;

@Getter
@Setter
public class UserDTO {

    private Integer id;

    private String email;

    private Long phone;

    private Date dob;

    private String firstName;

    private String lastName;

    private Gender gender;

    public UserDTO(User user) {
        BeanUtils.copyProperties(user, this);
    }
}
