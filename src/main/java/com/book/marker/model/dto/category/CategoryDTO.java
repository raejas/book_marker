package com.book.marker.model.dto.category;

import com.book.marker.model.entity.Category;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
public class CategoryDTO {

    private Integer id;

    private String name;

    private Short level;

    private Short parent;

    private String description;

    public CategoryDTO(Category category) {
        BeanUtils.copyProperties(category, this);
    }
}
