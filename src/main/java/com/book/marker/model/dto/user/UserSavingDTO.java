package com.book.marker.model.dto.user;

import com.book.marker.enumeration.Gender;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserSavingDTO {

    private String email;

    private Long phone;

    private Date dob;

    private String firstName;

    private String lastName;

    private Gender gender;
}
