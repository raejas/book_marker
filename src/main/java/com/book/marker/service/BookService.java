package com.book.marker.service;

import com.book.marker.model.dto.book.BookCreationDTO;
import com.book.marker.model.dto.book.BookDTO;
import com.book.marker.model.dto.book.BookUpdatedDTO;
import com.book.marker.model.entity.Book;
import javassist.NotFoundException;

import java.util.List;
import java.util.Set;

public interface BookService {

    BookDTO create(BookCreationDTO creationDTO);

    List<BookDTO> createMultiBooksByName(Set<String> names);

    List<BookDTO> getBookList();

    Book getById(Integer bookId) throws NotFoundException;

    BookDTO update(Integer bookId, BookUpdatedDTO updatedDTO) throws NotFoundException;

    boolean delete(Integer bookId) throws NotFoundException;
}
