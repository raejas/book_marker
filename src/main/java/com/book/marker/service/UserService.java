package com.book.marker.service;

import com.book.marker.model.dto.user.UserDTO;
import com.book.marker.model.dto.user.UserSavingDTO;
import javassist.NotFoundException;

import java.util.List;

public interface UserService {

    UserDTO create(UserSavingDTO savingDTO);

    List<UserDTO> getUserList();

    UserDTO getUserById(Integer userId) throws NotFoundException;

    UserDTO updateUser(Integer userId, UserSavingDTO savingDTO) throws NotFoundException;

    boolean deleteUser(Integer userId) throws NotFoundException;
}
