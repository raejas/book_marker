package com.book.marker.service;

import com.book.marker.model.dto.tag.TagCreationDTO;
import com.book.marker.model.dto.tag.TagDTO;
import com.book.marker.model.dto.tag.TagUpdatedDTO;
import com.book.marker.model.entity.Tag;
import javassist.NotFoundException;

import java.util.List;
import java.util.Set;

public interface TagService {

    TagDTO create(String name);

    void createMultiTags(Set<Tag> tags);

    List<TagDTO> getListTag();

    TagDTO getTagById(Integer tagId) throws NotFoundException;

    TagDTO updateTag(Integer tagId, String name) throws NotFoundException;

    boolean deleteTag(Integer tagId) throws NotFoundException;
}
