package com.book.marker.service;

import com.book.marker.model.dto.book.BookDetailDTO;
import com.book.marker.model.dto.book.BookDetailSavingDTO;
import javassist.NotFoundException;

public interface BookDetailService {

    BookDetailDTO findByBookId(Integer bookId) throws NotFoundException;

    BookDetailDTO createBookDetail(Integer bookId, BookDetailSavingDTO savingDTO) throws NotFoundException;

    BookDetailDTO updateBookDetail(Integer bookId, BookDetailSavingDTO savingDTO) throws NotFoundException;
}

