package com.book.marker.service;

import com.book.marker.model.dto.category.CategoryDTO;
import com.book.marker.model.dto.category.CategorySavingDTO;
import com.book.marker.model.entity.Category;
import javassist.NotFoundException;

import java.util.List;

public interface CategoryService {

    CategoryDTO create(CategorySavingDTO savingDTO);

    void createCategory(Category category);

    List<CategoryDTO> getCategoryList();

    CategoryDTO getById(Integer categoryId) throws NotFoundException;

    CategoryDTO update(Integer categoryId, CategorySavingDTO savingDTO) throws NotFoundException;

    boolean deleteCategory(Integer categoryId) throws NotFoundException;
}
