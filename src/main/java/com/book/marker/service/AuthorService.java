package com.book.marker.service;

import com.book.marker.model.dto.author.AuthorSavingDTO;
import com.book.marker.model.dto.author.AuthorDTO;
import com.book.marker.model.entity.Author;
import javassist.NotFoundException;

import java.util.List;
import java.util.Set;

public interface AuthorService {

    AuthorDTO createAuthors(AuthorSavingDTO savingDTO);

    void createMultiAuthorsByNames(Set<String> name);

    void createAuthors(Set<Author> authors);

    List<AuthorDTO> getListAuthor();

    AuthorDTO getById(Integer authorId) throws NotFoundException;

    AuthorDTO updateAuthor(Integer authorId, AuthorSavingDTO savingDTO) throws NotFoundException;

    boolean deleteAuthor(Integer authorId) throws NotFoundException;
}
