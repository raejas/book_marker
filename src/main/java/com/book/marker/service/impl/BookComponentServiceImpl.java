package com.book.marker.service.impl;

import com.book.marker.model.BookTableContent;
import com.book.marker.model.dto.book.BookComponentSavingDTO;
import com.book.marker.model.entity.Book;
import com.book.marker.model.entity.BookComponent;
import com.book.marker.repository.BookComponentRepository;
import com.book.marker.service.BookComponentService;
import com.book.marker.service.BookService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookComponentServiceImpl implements BookComponentService {

    private final BookComponentRepository repository;

    private final BookService bookService;

    @Override
    public BookTableContent getByBookId(Integer bookId) throws NotFoundException {
        Book book = bookService.getById(bookId);
        Set<BookComponent> components = book.getBookComponents();
        return new BookTableContent(book.getId(), book.getName(),components);
    }

    @Override
    public BookTableContent createBookComponent(Integer bookId, Set<BookComponentSavingDTO> savingDTOs) throws NotFoundException {
        Book book = bookService.getById(bookId);
        Set<BookComponent> bookComponents = savingDTOs.stream()
                .map(dto -> new BookComponent(dto, book))
                .collect(Collectors.toSet());
        repository.saveAll(bookComponents);
        return new BookTableContent(book.getId(), book.getName(), bookComponents);
    }

    @Override
    public BookTableContent updateBookComponent(BookTableContent tableContent) {
        Set<BookComponent> bookComponents = tableContent.getComponents();
        repository.saveAll(bookComponents);
        return tableContent;
    }
}
