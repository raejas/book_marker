package com.book.marker.service.impl;

import com.book.marker.model.dto.author.AuthorSavingDTO;
import com.book.marker.model.dto.author.AuthorDTO;
import com.book.marker.model.entity.Author;
import com.book.marker.repository.AuthorRepository;
import com.book.marker.service.AuthorService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {
    
    private final AuthorRepository repository;
    
    @Override
    public AuthorDTO createAuthors(AuthorSavingDTO savingDTO) {
        Author author = new Author();
        BeanUtils.copyProperties(savingDTO, author);
        repository.save(author);
        return new AuthorDTO(author);
    }

    @Override
    public void createMultiAuthorsByNames(Set<String> names) {
        Set<Author> authors = names.stream()
                .map(name -> new Author(name))
                .collect(Collectors.toSet());
        repository.saveAll(authors);
    }

    @Override
    public void createAuthors(Set<Author> authors) {
        if (CollectionUtils.isEmpty(authors)) {
            return;
        }

        validateCreation(authors);
        repository.saveAll(authors);
    }

    private void validateCreation(Set<Author> authors) {

    }

    @Override
    public List<AuthorDTO> getListAuthor() {
        List<Author> authorList = repository.findAll();
        return authorList.stream()
                .map(author -> new AuthorDTO(author))
                .collect(Collectors.toList());
    }

    @Override
    public AuthorDTO getById(Integer authorId) throws NotFoundException {
        Author author = repository.findById(authorId)
                .orElseThrow(() -> new NotFoundException("Cannot find author by Id: " + authorId));
        return new AuthorDTO(author);
    }

    @Override
    public AuthorDTO updateAuthor(Integer authorId, AuthorSavingDTO savingDTO) throws NotFoundException {
        Author existedAuthor = repository.findById(authorId)
                .orElseThrow(() -> new NotFoundException("Cannot find author by id: " + authorId));
        BeanUtils.copyProperties(savingDTO, existedAuthor);
        return new AuthorDTO(existedAuthor);
    }

    @Override
    public boolean deleteAuthor(Integer authorId) throws NotFoundException {
        Author existedAuthor = repository.findById(authorId)
                .orElseThrow(() -> new NotFoundException("Cannot find author by id: " + authorId));
        repository.delete(existedAuthor);
        return true;
    }
}
