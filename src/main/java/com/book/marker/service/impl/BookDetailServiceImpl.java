package com.book.marker.service.impl;

import com.book.marker.model.dto.book.BookDetailDTO;
import com.book.marker.model.dto.book.BookDetailSavingDTO;
import com.book.marker.model.entity.Book;
import com.book.marker.model.entity.BookDetail;
import com.book.marker.repository.BookDetailRepository;
import com.book.marker.service.BookDetailService;
import com.book.marker.service.BookService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookDetailServiceImpl implements BookDetailService {

    private final BookDetailRepository repository;

    private final BookService bookService;

    @Override
    public BookDetailDTO findByBookId(Integer bookId) throws NotFoundException {
        Book book = bookService.getById(bookId);
        return new BookDetailDTO(book);
    }

    @Override
    public BookDetailDTO createBookDetail(Integer bookId, BookDetailSavingDTO savingDTO) throws NotFoundException {
        Book book = bookService.getById(bookId);
        BookDetail detail = new BookDetail();
        BeanUtils.copyProperties(savingDTO, detail);
        detail.setBook(book);
        repository.save(detail);
        return new BookDetailDTO(book);
    }

    @Override
    public BookDetailDTO updateBookDetail(Integer bookId, BookDetailSavingDTO savingDTO) throws NotFoundException {
        Book book = bookService.getById(bookId);
        BookDetail detail = new BookDetail();
        BeanUtils.copyProperties(savingDTO, detail);
        repository.save(detail);
        return new BookDetailDTO(book);
    }
}
