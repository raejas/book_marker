package com.book.marker.service.impl;

import com.book.marker.model.dto.user.UserDTO;
import com.book.marker.model.dto.user.UserSavingDTO;
import com.book.marker.model.entity.User;
import com.book.marker.repository.UserRepository;
import com.book.marker.service.UserService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Override
    public UserDTO create(UserSavingDTO savingDTO) {
        User user = new User();
        BeanUtils.copyProperties(savingDTO, user);
        repository.save(user);
        return new UserDTO(user);
    }

    @Override
    public List<UserDTO> getUserList() {
        return repository.findAll().stream()
                .map(user -> new UserDTO(user))
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO getUserById(Integer userId) {
        User existed = repository.findById(userId)
                .orElse(null);
        return new UserDTO(existed);
    }

    @Override
    public UserDTO updateUser(Integer userId, UserSavingDTO savingDTO) throws NotFoundException {
        User existed = repository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Cannot find user with id: " + userId));
        BeanUtils.copyProperties(savingDTO, existed);
        repository.save(existed);
        return new UserDTO(existed);
    }

    @Override
    public boolean deleteUser(Integer userId) throws NotFoundException {
        User existed = repository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Cannot find user with id: " + userId));
        repository.delete(existed);
        return true;
    }
}
