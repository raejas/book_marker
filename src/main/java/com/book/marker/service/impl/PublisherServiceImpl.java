package com.book.marker.service.impl;

import com.book.marker.model.dto.publisher.PublisherDTO;
import com.book.marker.model.dto.publisher.PublisherSavingDTO;
import com.book.marker.model.entity.Book;
import com.book.marker.model.entity.Publisher;
import com.book.marker.repository.PublisherRepository;
import com.book.marker.service.PublisherService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PublisherServiceImpl implements PublisherService {

    private final PublisherRepository repository;

    @Override
    public PublisherDTO create(PublisherSavingDTO savingDTO) {
        Publisher publisher = new Publisher();
        BeanUtils.copyProperties(savingDTO, publisher);
        repository.save(publisher);
        return new PublisherDTO(publisher);
    }

    @Override
    public void createMultiPublisherByNames(Set<String> names) {
        Set<Publisher> publishers = names.stream()
                .map(name -> new Publisher(name))
                .collect(Collectors.toSet());
        repository.saveAll(publishers);
    }

    @Override
    public void create(Publisher publisher) {
        if (Objects.isNull(publisher) && Objects.isNull(publisher.getId())) {
            validatePublisherCreation(publisher);
            repository.save(publisher);
        }
    }

    private void validatePublisherCreation(Publisher publisher) {
        // Cannot duplicate name
    }

    @Override
    public List<Publisher> getListPublisher() {
        return repository.findAll();
    }

    @Override
    public Set<Book> getListPublisherBooks(Integer publisherId) {
        Publisher publisher = repository.findById(publisherId).get();
        return publisher.getBooks();
    }

    @Override
    public PublisherDTO getById(Integer publisherId) {
        PublisherDTO dto = new PublisherDTO(repository.getOne(publisherId));
        return dto;
    }

    @Override
    public PublisherDTO update(Integer publisherId, PublisherSavingDTO savingDTO) throws NotFoundException {
        Publisher existed = repository.findById(publisherId)
                .orElseThrow(() -> new NotFoundException("Publisher not existed"));
        BeanUtils.copyProperties(savingDTO, existed, "id");
        repository.save(existed);
        return new PublisherDTO(existed);
    }

    @Override
    public boolean delete(Integer publisherId) throws NotFoundException {
        Publisher existed = repository.findById(publisherId)
                .orElseThrow(() -> new NotFoundException("Publisher not existed"));
        repository.delete(existed);
        return true;
    }
}
