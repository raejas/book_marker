package com.book.marker.service.impl;

import com.book.marker.model.dto.book.BookCreationDTO;
import com.book.marker.model.dto.book.BookDTO;
import com.book.marker.model.dto.book.BookUpdatedDTO;
import com.book.marker.model.entity.Author;
import com.book.marker.model.entity.Book;
import com.book.marker.model.entity.BookDetail;
import com.book.marker.model.entity.Tag;
import com.book.marker.repository.BookRepository;
import com.book.marker.service.AuthorService;
import com.book.marker.service.BookService;
import com.book.marker.service.CategoryService;
import com.book.marker.service.PublisherService;
import com.book.marker.service.TagService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository repository;

    private final AuthorService authorService;

    private final PublisherService publisherService;

    private final CategoryService categoryService;

    private final TagService tagService;

    @Override
    public BookDTO create(BookCreationDTO creationDTO) {
        Book book = new Book();
        validateBookCreation(creationDTO);
        BeanUtils.copyProperties(creationDTO, book);

        Set<Author> addedAuthors = creationDTO.getAuthors().stream()
                .filter(author -> Objects.isNull(author.getId()))
                .collect(Collectors.toSet());
        authorService.createAuthors(addedAuthors);
        publisherService.create(creationDTO.getPublisher());
        categoryService.createCategory(creationDTO.getCategory());
        Set<Tag> addedTags = creationDTO.getTags().stream()
                .filter(tag -> Objects.isNull(tag.getId()))
                .collect(Collectors.toSet());
        tagService.createMultiTags(addedTags);

        if (Objects.nonNull(creationDTO.getBookDetail())) {
            BookDetail detail = creationDTO.getBookDetail();
            detail.setBook(book);
        }

        repository.save(book);
        return new BookDTO(book);
    }

    @Override
    public List<BookDTO> createMultiBooksByName(Set<String> names) {
        Set<Book> books = names.stream()
                .map(name -> new Book(name))
                .collect(Collectors.toSet());
        List<BookDTO> dtos = repository.saveAll(books).stream()
                .map(book -> new BookDTO(book))
                .collect(Collectors.toList());
        return dtos;
    }

    @Override
    public List<BookDTO> getBookList() {
        return repository.findAll().stream()
                .map(book -> new BookDTO(book))
                .collect(Collectors.toList());
    }

    @Override
    public Book getById(Integer bookId) throws NotFoundException{
        Book book = repository.findById(bookId)
                .orElseThrow(() -> new NotFoundException("Cannot find book with Id: "));
        return book;
    }

    @Override
    public BookDTO update(Integer bookId, BookUpdatedDTO updatedDTO) throws NotFoundException {
        Book existed = repository.findById(bookId)
                .orElseThrow(() -> new NotFoundException("Cannot find book with Id: "));
        BeanUtils.copyProperties(updatedDTO, existed);
        repository.save(existed);
        return new BookDTO(existed);
    }

    @Override
    public boolean delete(Integer bookId) throws NotFoundException {
        Book existed = repository.findById(bookId)
                .orElseThrow(() -> new NotFoundException("Cannot find book with Id: "));
        repository.delete(existed);
        return true;
    }

    private void validateBookCreation(BookCreationDTO dto) {
        // Cannot duplicate name with version and publisher and author
    }
}
