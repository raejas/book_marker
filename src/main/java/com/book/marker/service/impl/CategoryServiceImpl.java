package com.book.marker.service.impl;

import com.book.marker.model.dto.category.CategoryDTO;
import com.book.marker.model.dto.category.CategorySavingDTO;
import com.book.marker.model.entity.Category;
import com.book.marker.repository.CategoryRepository;
import com.book.marker.service.CategoryService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository repository;

    @Override
    public CategoryDTO create(CategorySavingDTO savingDTO) {
        Category category = new Category();
        BeanUtils.copyProperties(savingDTO, category);
        repository.save(category);
        return new CategoryDTO(category);
    }

    @Override
    public void createCategory(Category category) {
        if (Objects.isNull(category)) {
            return;
        }

        validateCreation(category);
        repository.save(category);
    }

    private void validateCreation(Category category) {
        if (repository.countByName(category.getName()) > 0) {

        }
    }

    @Override
    public List<CategoryDTO> getCategoryList() {
        return repository.findAll().stream()
                .map(category -> new CategoryDTO(category))
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDTO getById(Integer categoryId) throws NotFoundException {
        Category category = repository.findById(categoryId)
                .orElseThrow(() -> new NotFoundException("Cannot find category with id: " + categoryId));
        return new CategoryDTO(category);
    }

    @Override
    public CategoryDTO update(Integer categoryId, CategorySavingDTO updatedDTO) throws NotFoundException {
        Category existed = repository.findById(categoryId)
                .orElseThrow(() -> new NotFoundException("Cannot find category with id: " + categoryId));
        BeanUtils.copyProperties(updatedDTO, categoryId);
        return new CategoryDTO(existed);
    }

    @Override
    public boolean deleteCategory(Integer categoryId) throws NotFoundException {
        Category existed = repository.findById(categoryId)
                .orElseThrow(() -> new NotFoundException("Cannot find category with id: " + categoryId));
        repository.delete(existed);
        return true;
    }
}
