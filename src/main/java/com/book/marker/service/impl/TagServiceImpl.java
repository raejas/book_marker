package com.book.marker.service.impl;

import com.book.marker.model.dto.tag.TagDTO;
import com.book.marker.model.entity.Tag;
import com.book.marker.repository.TagRepository;
import com.book.marker.service.TagService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository repository;

    @Override
    public TagDTO create(String name) {
        Tag tag = new Tag(name);
        repository.save(tag);
        return new TagDTO(tag);
    }

    @Override
    public void createMultiTags(Set<Tag> tags) {
        if (!CollectionUtils.isEmpty(tags)) {
            return;
        }
        validateTagCreation(tags);
        repository.saveAll(tags);
    }

    private void validateTagCreation(Set<Tag> addedTags) {
        // Cannot duplicate name
    }

    @Override
    public List<TagDTO> getListTag() {
        return repository.findAll().stream()
                .map(tag -> new TagDTO(tag))
                .collect(Collectors.toList());
    }

    @Override
    public TagDTO getTagById(Integer tagId) throws NotFoundException {
        Tag existedTag = repository.findById(tagId)
                .orElseThrow(() -> new NotFoundException("Cannot find tag with id: " + tagId));
        return new TagDTO(existedTag);
    }

    @Override
    public TagDTO updateTag(Integer tagId, String name) throws NotFoundException {
        Tag existedTag = repository.findById(tagId)
                .orElseThrow(() -> new NotFoundException("Cannot find tag with id: " + tagId));
        existedTag.setName(name);
        return new TagDTO(existedTag);
    }

    @Override
    public boolean deleteTag(Integer tagId) throws NotFoundException {
        Tag existedTag = repository.findById(tagId)
                .orElseThrow(() -> new NotFoundException("Cannot find tag with id: " + tagId));
        repository.delete(existedTag);
        return false;
    }
}
