package com.book.marker.service;

import com.book.marker.model.BookTableContent;
import com.book.marker.model.dto.book.BookComponentSavingDTO;
import javassist.NotFoundException;

import java.util.Set;

public interface BookComponentService {

    BookTableContent getByBookId(Integer bookId) throws NotFoundException;

    BookTableContent createBookComponent(Integer bookId, Set<BookComponentSavingDTO> savingDTOs) throws NotFoundException;

    BookTableContent updateBookComponent(BookTableContent tableContent) throws NotFoundException;
}
