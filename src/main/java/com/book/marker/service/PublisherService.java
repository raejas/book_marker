package com.book.marker.service;

import com.book.marker.model.dto.publisher.PublisherDTO;
import com.book.marker.model.dto.publisher.PublisherSavingDTO;
import com.book.marker.model.entity.Book;
import com.book.marker.model.entity.Publisher;
import javassist.NotFoundException;

import java.util.List;
import java.util.Set;

public interface PublisherService {

    PublisherDTO create(PublisherSavingDTO savingDTO);

    void createMultiPublisherByNames(Set<String> names);

    void create(Publisher publisher);

    List<Publisher> getListPublisher();

    Set<Book> getListPublisherBooks(Integer publisherId);

    PublisherDTO getById(Integer publisherId);

    PublisherDTO update(Integer publisherId, PublisherSavingDTO savingDTO) throws NotFoundException;

    boolean delete(Integer publisherId) throws NotFoundException;
}
