package com.book.marker.repository;

import com.book.marker.model.entity.BookDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookDetailRepository extends JpaRepository<BookDetail, Integer> {

    BookDetail findByBookId(Integer bookId);
}
