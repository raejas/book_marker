package com.book.marker.repository;

import com.book.marker.model.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {

    Long countAllByNameIn(List<String> authorNames);

    Set<Author> findByNameIn(Set<String> authorNames);

    Set<Author> findByIdIn(Set<Integer> authorIds);
}
