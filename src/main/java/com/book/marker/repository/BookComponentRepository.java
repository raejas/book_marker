package com.book.marker.repository;

import com.book.marker.model.entity.BookComponent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookComponentRepository extends JpaRepository<BookComponent, Integer> {
}
