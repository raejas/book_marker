package com.book.marker.repository;

import com.book.marker.model.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Long countByName(String name);
}
